import React, { Component } from 'react';
import '../style/signupForm.css';
import SignupSuccess from './signupSuccess';
import Joi from "joi";
import PasswordComplexity from 'joi-password-complexity';

class SignupForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {
                first_name: '',
                last_name: '',
                age: '',
                gender: '',
                role: '',
                email: '',
                password: '',
                password_confirmation: '',
                isAgree: false,
            },
            errors: {},
            isSubmitted: false,
        }

        this.schema = Joi.object({
            first_name: Joi.string()
                .alphanum()
                .required()
                .messages({
                    "string.empty": "First Name is required.",
                }),

            last_name: Joi.string()
                .alphanum()
                .required()
                .messages({
                    "string.empty": "Last Name is required.",
                }),

            age: Joi.number()
                .required()
                .min(1)
                .max(120)
                .integer(),

            gender: Joi.string()
                .required(),

            role: Joi.string()
                .required()
                .messages({
                    "string.empty": "Role is required.",
                }),

            email: Joi.string()
                .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net', 'in', 'io'] } })
                .required()
                .messages({
                    "string.email": "Please enter a valid email address.",
                    "string.empty": "Email address is required.",
                }),

            password: new PasswordComplexity({
                min: 6,
                max: 30,
                lowerCase: 1,
                upperCase: 1,
                numeric: 1,
                symbol: 1,
                requirementCount: 4,
            }).required(),

            password_confirmation: Joi.string()
                .required()
                .valid(Joi.ref('password'))
                .messages({
                    "string.empty": "Confirm password is required",
                    "any.only": "Password must match"
                }),

            isAgree: Joi.any()
                .valid(true)
                .messages({
                    "any.only": "Please agree before submitting"
                }),
        });

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = (target.type === 'checkbox') ? target.checked : target.value;
        const name = target.name;

        const user = { ...this.state.user };
        const errors = { ...this.state.errors };

        if (errors.hasOwnProperty(name)) {
            delete errors[name];
        }

        user[name] = value;
        const errorsData = this.validateProperty(user);

        if (errorsData) {
            errorsData.map((error) => {
                if (error.path[0] === name) {
                    if (errors[name]) {
                        errors[name] += ', ' + error.message;
                    } else {
                        errors[name] = error.message;
                    }
                }
            })
        }

        this.setState({
            user,
            errors,
        }, () => {
            console.log(this.state)

        });
    }

    validateProperty = (user) => {
        const result = this.schema.validate(user, {
            abortEarly: false
        });
        console.log(result)
        const { error } = result;
        return error ? error.details : null;
    }

    handleFormSubmit(event) {
        event.preventDefault();

        const user = { ...this.state.user };
        const errors = { ...this.state.errors };
        const errorsTemp = {};
        let isSubmitted = this.state.isSubmitted;

        const errorsData = this.validateProperty(user);

        if (errorsData) {
            errorsData.map((error) => {
                const name = error.path[0];
                if (errorsTemp[name]) {
                    errorsTemp[name] += ', ' + error.message;
                } else {
                    errorsTemp[name] = error.message;
                }
            })
        }

        isSubmitted = (Object.keys(errorsTemp).length === 0)

        this.setState({
            user,
            errors: errorsTemp,
            isSubmitted,
        }, () => {
            console.log(this.state)

        });

    }

    render() {

        const userDetails = this.state.user;
        const errors = this.state.errors;

        return (
            <div className="container my-3 p-3">
                {(!this.state.isSubmitted)
                    ? <form className="signupForm">
                        <h2 className='title mb-4'>Signup Form</h2>
                        <div className='row g-3 gx-5'>
                            <div className='col-md-6 mb-2'>
                                <label
                                    htmlFor="first_name"
                                    className="form-label">
                                    First Name
                                    <i className="fa-solid fa-user"></i>
                                </label>
                                <input
                                    type="string"
                                    className={"form-control" + " " +
                                        (errors.first_name
                                            ? "is-invalid"
                                            : userDetails.first_name
                                                ? "is-valid"
                                                : "")}
                                    name="first_name"
                                    onChange={this.handleInputChange}
                                    value={userDetails.first_name}
                                    placeholder="Enter your first name"
                                    id="first_name"
                                />
                                <div className="invalid-feedback">
                                    {errors.first_name}
                                </div>
                            </div>
                            <div className='col-md-6 mb-2'>
                                <label
                                    htmlFor="last_name"
                                    className="form-label">
                                    Last Name
                                    <i className="fa-solid fa-user"></i>
                                </label>
                                <input
                                    type="string"
                                    className={"form-control" + " " +
                                        (errors.last_name
                                            ? "is-invalid"
                                            : userDetails.last_name
                                                ? "is-valid"
                                                : "")}
                                    name="last_name"
                                    value={userDetails.last_name}
                                    placeholder="Enter your last name"
                                    onChange={this.handleInputChange}
                                    id="last_name"
                                />
                                <div className="invalid-feedback">
                                    {errors.last_name}
                                </div>
                            </div>
                            <div className='col-md-6 mb-2'>

                                <label
                                    htmlFor="age"
                                    className="form-label">
                                    Age
                                    <i className="fa-solid fa-calendar"></i>
                                </label>

                                <input
                                    type="number"
                                    className={"form-control" + " " +
                                        (errors.age
                                            ? "is-invalid"
                                            : userDetails.age
                                                ? "is-valid"
                                                : "")}
                                    name="age"
                                    value={userDetails.age}
                                    placeholder="Enter your age"
                                    onChange={this.handleInputChange}
                                    id="age"
                                />
                                <div className="invalid-feedback">
                                    {errors.age}
                                </div>
                            </div>
                            <div className='col-md-6 mb-2'>
                                <label className="form-label">
                                    Gender
                                    <i className="fa-solid fa-person"></i>
                                </label>
                                <div>
                                    <div className="form-check form-check-inline">
                                        <input
                                            className="form-check-input"
                                            type="radio"
                                            name="gender"
                                            id="inlineRadio1"
                                            value="male"
                                            onChange={this.handleInputChange}
                                        />
                                        <label
                                            className="form-check-label"
                                            htmlFor="inlineRadio1">
                                            Male
                                        </label>
                                    </div>
                                    <div className="form-check form-check-inline">
                                        <input
                                            className="form-check-input"
                                            type="radio"
                                            name="gender"
                                            id="inlineRadio2"
                                            value="female"
                                            onChange={this.handleInputChange}
                                        />
                                        <label
                                            className="form-check-label"
                                            htmlFor="inlineRadio2">
                                            Female
                                        </label>
                                    </div>
                                    <div className="form-check form-check-inline">
                                        <input
                                            className="form-check-input"
                                            type="radio"
                                            name="gender"
                                            id="inlineRadio3"
                                            value="other"
                                            onChange={this.handleInputChange}
                                        />
                                        <label
                                            className="form-check-label"
                                            htmlFor="inlineRadio3">
                                            Other
                                        </label>
                                    </div>
                                </div>
                                {errors.gender
                                    && <div className="errorMessage">
                                        {errors.gender}
                                    </div>}
                            </div>
                            <div className="mb-2">
                                <label
                                    htmlFor="role"
                                    className="form-label">
                                    Role
                                    <i className="fa-solid fa-briefcase"></i>
                                </label>
                                <select
                                    name="role"
                                    value={userDetails.role}
                                    onChange={this.handleInputChange}
                                    className={"form-select" + " " +
                                        (errors.role
                                            ? "is-invalid"
                                            : userDetails.role
                                                ? "is-valid"
                                                : "")}
                                    id="role">
                                    <option value="">Select your current role</option>
                                    <option value="developer">Developer</option>
                                    <option value="senior developer">Senior Developer</option>
                                    <option value="lead engineer">Lead Engineer</option>
                                    <option value="CTO">CTO</option>
                                </select>
                                <div className="invalid-feedback">
                                    {errors.role}
                                </div>
                            </div>
                            <div className='mb-2'>
                                <label
                                    htmlFor="email"
                                    className="form-label">
                                    Email address
                                    <i className="fa-solid fa-envelope"></i>
                                </label>
                                <input
                                    type="email"
                                    className={"form-control" + " " +
                                        (errors.email
                                            ? "is-invalid"
                                            : userDetails.email
                                                ? "is-valid"
                                                : "")}
                                    name="email"
                                    value={userDetails.email}
                                    placeholder="Enter your Email address"
                                    onChange={this.handleInputChange}
                                    id="email"
                                />
                                <div className="invalid-feedback">
                                    {errors.email}
                                </div>
                            </div>
                            <div className='mb-2'>
                                <label
                                    htmlFor="password"
                                    className="form-label">
                                    Password
                                    <i className="fa-solid fa-lock"></i>
                                </label>
                                <input
                                    type="password"
                                    className={"form-control" + " " +
                                        (errors.password
                                            ? "is-invalid"
                                            : userDetails.password
                                                ? "is-valid"
                                                : "")}
                                    placeholder="Enter a new password (min- 6 characters)"
                                    name="password"
                                    value={userDetails.password}
                                    onChange={this.handleInputChange}
                                    id="password"
                                />
                                <div className="invalid-feedback">
                                    {errors.password}
                                </div>
                            </div>
                            <div className='mb-2'>
                                <label
                                    htmlFor="password_confirmation"
                                    className="form-label">
                                    Confirm Password
                                    <i className="fa-solid fa-unlock"></i>
                                </label>
                                <input
                                    type="password"
                                    className={"form-control" + " " +
                                        (errors.password_confirmation
                                            ? "is-invalid"
                                            : userDetails.password_confirmation
                                                ? "is-valid"
                                                : "")}
                                    placeholder="Confirm Password"
                                    name="password_confirmation"
                                    value={userDetails.password_confirmation}
                                    onChange={this.handleInputChange}
                                    id="password_confirmation"
                                />
                                <div className="invalid-feedback">
                                    {errors.password_confirmation}
                                </div>
                            </div>
                            <div className="form-check mb-4">
                                <input
                                    className={"form-check-input ms-2" + " " +
                                        (errors.isAgree
                                            ? "is-invalid"
                                            : '')}
                                    name="isAgree"
                                    type="checkbox"
                                    checked={userDetails.isAgree}
                                    onChange={this.handleInputChange}
                                    id="isAgree"
                                />
                                <label
                                    className="form-check-label ps-2"
                                    htmlFor="isAgree">
                                    Agree to terms and conditions
                                </label>
                                <div className="invalid-feedback ms-2">
                                    {errors.isAgree}
                                </div>
                            </div>
                            <button
                                onClick={this.handleFormSubmit}
                                className="btn btn-primary w-50 m-auto"
                            >
                                Submit
                            </button>
                        </div>
                    </form>
                    : <SignupSuccess />
                }

            </div>

        )
    }
}
export default SignupForm;
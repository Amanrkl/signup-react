import React from 'react';

function SignupSuccess() {
    return (
        <div className='SuccessMessage'>
            <h1>Your Application was successfully submitted.</h1>
        </div>
    );
}

export default SignupSuccess;
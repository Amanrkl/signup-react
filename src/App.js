import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import SignupForm from './components/signupForm';

function App() {
  return (
    <div className="App">
      <div className='container form1'>
        <div className='row'>
          <div className='logo col-md-4'></div>
          <div className='col-md-8'>
            <SignupForm />
          </div>
        </div>

      </div>
    </div>
  );
}

export default App;
